import java.io.FileInputStream;
import java.util.Properties;

import org.testng.annotations.BeforeSuite;

public class DriverInstance {

	private Properties prop;
	private FileInputStream inputstream;
	
	@BeforeSuite
	public void DriverInstanceTest() throws Exception {
		System.out.println("Test Suite");
		prop = new Properties();
		inputstream = new FileInputStream("data.properties");
		prop.load(inputstream);
		System.out.println(prop.getProperty("browser"));
	}
}
