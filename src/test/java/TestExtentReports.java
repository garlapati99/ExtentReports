import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class TestExtentReports extends DriverInstance{
	
	private WebDriver driver;
	
	public ExtentHtmlReporter htmlreporter;
	public ExtentReports extent;
	public ExtentTest test;
	

	@Test
	public void doLogin()  throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "/Users/garlapati/Downloads/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://velocity.ocbc.com/login.html");
		TimeUnit.SECONDS.sleep(3);
		
		//index=2
		
		List<WebElement> ele = driver.findElements(By.tagName("iframe"));
		System.out.println("Number of Frames in the page "+ele.size());
		
		for(WebElement el: ele) {
			System.out.println("Frame id "+el.getAttribute("id"));
			System.out.println("Frame  name "+el.getAttribute("name"));
		}
		
		
		driver.switchTo().frame("dpms-iframe");
		
		TimeUnit.SECONDS.sleep(3);
		
		driver.findElement(By.id("fieldtxt0")).click();
		new Select(driver.findElement(By.id("fieldtxt0"))).selectByVisibleText("Singapore");
		
		TimeUnit.SECONDS.sleep(3);
		
		List<WebElement> ele1 = driver.findElements(By.tagName("iframe"));
		System.out.println("Number of Frames in the page "+ele1.size());
		
		for(WebElement el: ele1) {
			System.out.println("Frame id "+el.getAttribute("id"));
			System.out.println("Frame  name "+el.getAttribute("name"));
		}
		
		System.out.println("test");
		
		driver.switchTo().frame("frame_2_8_content");
		
		TimeUnit.SECONDS.sleep(3);
		
		driver.findElement(By.id("property1")).click();
	    driver.findElement(By.id("property1")).clear();
	    driver.findElement(By.id("property1")).sendKeys("test");
	    driver.findElement(By.name("loginId")).click();
	    driver.findElement(By.name("loginId")).clear();
	    driver.findElement(By.name("loginId")).sendKeys("test");
	    driver.findElement(By.name("loginPassword")).click();
	    driver.findElement(By.name("loginPassword")).clear();
	    driver.findElement(By.name("loginPassword")).sendKeys("test");
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password/ Unlock User'])[1]/following::div[4]")).click();
	    
	    TimeUnit.SECONDS.sleep(5);
		
		
		driver.quit();
		
	}

}
